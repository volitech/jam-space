const Server = require("./Server");

class Main {
    constructor() {
        const server = new Server();

        void server.listen();
    }
}

new Main();
