const path    = require("path");
const express = require("express");
const http    = require("http");
const io      = require("socket.io");

const EpiStellar = require("./EpiStellar.js");

class Server {
    constructor() {
        this.app    = express();
        this.server = http.createServer(this.app);
        this.ws     = io(this.server);
        this.game   = new EpiStellar();

        this.app.use((req, res, next) => {
            if (req.path == "/")
                return res.status(200).sendFile(
                    path.resolve("./views/index.html")
                );
            next();
        });
        this.app.use(express.static("./views"));
        this.app.use((req, res) => {
            return res.status(404).sendFile(
                path.resolve("views/404.html")
            );
        });

        this.ws.on("connection", socket => {
            socket.on("init", (username, skin_id) => {
                socket.isInit = true;
                this.game.addClient(socket, username, skin_id);
            });
            socket.on("upd", data => {
                if (!socket.isInit)
                    return;
                this.game.updatePlayer(socket.player.id, data);
            });
            socket.on("shoot", data => {
                if (!socket.isInit)
                    return;
                this.game.shoot(socket.player.id, data);
            });
            socket.on("hit", (bullet, player) => {
                if (!socket.isInit)
                    return;
                this.game.checkHit(socket.player.id, bullet, player);
            });
            socket.on("disconnect", () => {
                if (socket.isInit)
                    this.game.removeClient(socket);
            });
        });
    }

    listen(port = 8080) {
        return new Promise(resolve => {
            this.server.listen(port, () => {
                console.log("EpiStellar server listening on port", port);
                resolve();
            });
        });
    }
}

module.exports = Server;
