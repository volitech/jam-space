const Vector2 = require("./Vector2.js");

const BULLET_LIFETIME   = 3e3;
const BULLET_SPEED      = 1;

class Bullet {
    constructor(skin_id, pos = new Vector2(), vel = new Vector2(), rot = 0) {
        const rot_vel = new Vector2(Math.sin(rot), -Math.cos(rot));

        this.pos      = pos.clone();
        this.vel      = vel.clone().add(rot_vel);
        this.rot      = rot;
        this.skin_id  = skin_id;
        this.lifetime = BULLET_LIFETIME;
    }

    update(dt) {
        this.pos.add(this.vel.clone().mult(dt * BULLET_SPEED));
        this.lifetime -= dt;
    }

    serialize() {
        return [
            this.id,
            this.pos.serialize(),
            this.vel.serialize(),
            Math.round(this.rot * 1e2) / 1e2,
            this.skin_id
        ];
    }

    unserialize(data) {
        if (
            data.length != 5 ||
            data[1].length != 2 ||
            data[2].length != 2
        )
            return false;
        this.id         = parseInt(data[0]);
        this.pos.x      = parseFloat(data[1][0]);
        this.pos.y      = parseFloat(data[1][1]);
        this.vel.x      = parseFloat(data[2][0]);
        this.vel.y      = parseFloat(data[2][1]);
        this.rot        = parseFloat(data[3]);
        this.skin_id    = parseFloat(data[4]);
        return true;
    }
}

module.exports = Bullet;
