const Player = require("./Player.js");
const Bullet = require("./Bullet.js");

const PLAYER_SIZE = 64;

class EpiStellar {
    constructor() {
        this.clients    = {};
        this.bullets    = {};
        this.prevTick   = new Date().getTime();
        this.lastId     = 1;

        setInterval(() => {
            const now = new Date().getTime();

            this.update(now - this.prevTick);
            this.prevTick = now;
        }, 50);
    }

    addClient(socket, username, skin_id) {
        socket.player           = new Player(username, skin_id);
        socket.player.id        = this.lastId++;
        socket.player.socket    = socket;
        this.broadcast(
            socket.player,
            null,
            "join",
            [[socket.player.id, username, skin_id]]
        );
        socket.emit("init", socket.player.id);
        socket.emit("join", Object.values(this.clients).map(
            client => ([
                client.player.id,
                client.player.username,
                client.player.skin_id
            ])
        ));
        this.clients[socket.player.id] = socket;
        console.log(`Player #${socket.player.id} connected`);
    }

    removeClient(socket) {
        this.broadcast(socket.player, null, "leave", [socket.player.id]);
        delete this.clients[socket.player.id];
        console.log(`Player #${socket.player.id} disconnected`);
    }

    broadcast(player, cond_fn, event, data) {
        for (const client of Object.values(this.clients)) {
            let other = client.player;

            if (other.id != player.id && (!cond_fn || cond_fn(other)))
                other.socket.emit(event, data);
        }
    }

    shoot(id, data) {
        if (!this.clients[id])
            return;
        const player = this.clients[id].player;
        const bullet = new Bullet(player.skin_id);

        if (!bullet.unserialize(data))
            return;
        bullet.id = player.id * 100 + bullet.id % 100;
        this.bullets[bullet.id] = bullet;
        this.broadcast(player, p => (player.isInRange(p)), "shoot", bullet.serialize());
    }

    checkHit(player_id, bullet_id, enemy_id) {
        bullet_id = player_id * 100 + bullet_id % 100;

        const player = this.clients[player_id].player;
        const enemy  = this.clients[enemy_id].player;
        const bullet = this.bullets[bullet_id];

        if (
            !player ||
            !enemy ||
            !bullet ||
            bullet.pos.clone().sub(enemy.pos).getDist() > PLAYER_SIZE
        )
            return;

        this.killPlayer(enemy, bullet_id);
    }

    killPlayer(player, bullet_id = -1) {
        player.isAlive = false;
        player.socket.emit("die", [0, bullet_id]);

        this.broadcast(player, null, "die", [player.id, bullet_id]);

        setTimeout(() => {
            player.socket.disconnect(true);
        }, 1e3);
    }

    updatePlayer(id, data) {
        if (!this.clients[id])
            return;
        const player = this.clients[id].player;

        if (!player.unserialize(data))
            return console.error("Failed to unserialize", data);

        player.socket.emit(
            "upd",
            Object.values(this.clients).map(cl => (cl.player.serialize()))
        );
    }

    update(dt) {
        for (const client of Object.values(this.clients)) {
            const player = client.player;

            player.update(dt);

            if (!player.isAlive)
                continue;
            if (Math.abs(player.pos.y) > 8100 || Math.abs(player.pos.x) > 12800)
                this.killPlayer(player);
        }

        for (const bullet of Object.values(this.bullets)) {
            bullet.update(dt);

            if (bullet.lifetime < 0)
                delete this.bullets[bullet.id];
        }
    }
}

module.exports = EpiStellar;
