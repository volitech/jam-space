const Vector2 = require("./Vector2.js");

const MAX_SPEED     = 0.75;
const PLAYER_RANGE  = 1e3;

class Player {
    constructor(username, skin_id = 0, pos = new Vector2()) {
        this.username       = username;
        this.skin_id        = skin_id;
        this.pos            = pos;
        this.vel            = new Vector2();
        this.rot            = 0;
        this.isAlive        = true;
        this.isThrustOn     = false;
        this.firing_times   = [0, 0];
        this.min_wp_delays  = [1e2, 1e4];
    }

    isInRange(other_player) {
        return this.pos.clone().sub(other_player.pos).getDist() <= PLAYER_RANGE;
    }

    shoot(is_primary = true) {
        const now = new Date().getTime();

        if (now - this.firing_times[0 + is_primary] < this.min_wp_delays[0 + is_primary])
            return null;
        this.firing_times[0 + is_primary] = now;
        return new Bullet(
            this.skin_id,
            this.pos,
            this.vel,
            this.rot
        );
    }

    update(dt) {
        this.vel.clampAbs(MAX_SPEED);
        if (!this.isAlive)
            return;
        this.pos.add(this.vel.clone().mult(dt));
    }

    serialize() {
        return [
            this.id,
            this.pos.serialize(),
            this.vel.serialize(),
            this.rot,
            0 + this.isThrustOn
        ];
    }

    unserialize(data) {
        if (
            data.length != 4 ||
            data[0].length != 2 ||
            data[1].length != 2
        )
            return false;
        this.pos.x  = parseFloat(data[0][0]);
        this.pos.y  = parseFloat(data[0][1]);
        this.vel.x  = parseFloat(data[1][0]);
        this.vel.y  = parseFloat(data[1][1]);
        this.rot    = parseFloat(data[2]);
        this.isThrustOn = Boolean(data[3]);
        return true;
    }
}

module.exports = Player;
