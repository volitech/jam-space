# ![EpiStellar](./views/assets/title.png)
EpiStellar is an online browser game. Its goal is pretty straight forward: play as a spaceship, fly around and shoot other players. The game requires no registration and can be run and played on a local network.

# Installation
1. Clone this repository and navigate into it
1. Install dependencies by running `npm i`
1. Launch the server by running `npm start`
1. Done! You can now navigate to `http://127.0.0.1:8080/` in your browser and start playing.

If you wish to play on your local network, tell other players to open the same URL, replacing `127.0.0.1` with your local IP address (make sure to be on the same network though!)

**Note**: *If other players are unable to connect, check your firewall: you may want to disable it or whitelist port `8080`*

# Controls
It's simple: move around with arrow keys, shoot with space.

| Action       | Key(s)         |
|--------------|----------------|
| Add thrust   | Up arrow, W    |
| Rotate left  | Left arrow, A  |
| Rotate right | Right arrow, D |
| Fire         | Space bar      |

# Authors
@volifter\
@FafnirMoebiusAlphari
