FROM node:23-alpine

WORKDIR /epistellar

COPY package*.json ./
RUN npm ci

COPY src ./src
COPY views ./views

EXPOSE 8080

CMD npm start
