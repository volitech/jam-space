const player =  {
    username: "",
    skin_id: 0
};

window.addEventListener("load", init);

function loadPlayer() {
    const data = window.localStorage.getItem("epistellar_player");

    if (!data)
        return;

    Object.assign(player, JSON.parse(data));

    document.querySelector(".player-name").value = player.username;
    setSkin(player.skin_id);
}

function savePlayer(data) {
    window.localStorage.setItem(
        "epistellar_player",
        JSON.stringify(Object.assign(player, data))
    );
}

function setSkin(i) {
    document.querySelector(".ships > .selected")?.classList?.remove("selected");
    document.querySelector(".ships").children[i].classList.add("selected");

    savePlayer({skin_id: i});
}

function init() {
    [...document.querySelector(".ships").children].forEach(
        ($el, i) => $el.addEventListener("click", () => setSkin(i))
    );

    document.querySelector(".play-button").addEventListener("click", start);
    document.addEventListener("keydown", e => {
        if (e.key == "Enter")
            start();
    });

    loadPlayer();
}

function start() {
    const username = document.getElementById("player-name").value;

    if (!username)
        return alert("Please enter a username");

    savePlayer({username});

    window.top.postMessage(player, "*");
}
