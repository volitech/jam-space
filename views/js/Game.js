const ROT_SPEED           = 5e-3;
const ACCELERATION        = 1e-3;
const SRV_UPDATE_INTERVAL = 100;
const PLAYER_SIZE         = 64;

const CONTROLS = {
    THRUST: ["ArrowUp", "w"],
    LEFT:   ["ArrowLeft", "a"],
    RIGHT:  ["ArrowRight", "d"],
    FIRE:   [" "]
};

let game;

class Game {
    constructor(username, skin_id) {
        this.app = new PIXI.Application({
            width: 800,
            height: 600,
            backgroundColor: 0x880033,
            view: document.getElementById("game-canvas"),
            resolution: 2,
            resizeTo: document.getElementById("game-div")
        });
        this.socket      = io(window.location.origin, {
            path: window.location.pathname + "socket.io/",
            transport: ["websocket", "polling"]
        });
        this.stage       = this.app.stage;
        this.pressedKeys = {};
        this.players     = {};
        this.bullets     = [];
        this.player      = new Player(0, username, skin_id);

        this.initSound();
        this.initBg();

        this.player.isMine = true;

        this.addPlayer(this.player);

        window.addEventListener("keydown", e => { this.pressedKeys[e.key] = true; });
        window.addEventListener("keyup", e => { this.pressedKeys[e.key] = false; });

        this.app.ticker.add(() => {
            this.update(this.app.ticker.elapsedMS);
        });

        this.initSocket(username, skin_id);
    }

    initBg() {
        const SIZE      = 32;
        const WIDTH     = 1594;
        const HEIGHT    = 997;
        const texture = PIXI.Texture.from("assets/bg.jpg");

        this.bg = new PIXI.Container();
        this.bg.scale.set(0.5);
        for (let y = 0; y < SIZE; y++) {
            for (let x = 0; x < SIZE; x++) {
                const tile = new PIXI.Sprite(texture);

                tile.anchor.set(0.8);

                tile.x      = x * WIDTH;
                tile.y      = y * HEIGHT;
                tile.width  = WIDTH;
                tile.height = HEIGHT;

                this.bg.addChild(tile);
            }
        }
        this.bg.x       = this.app.screen.width / 2;
        this.bg.y       = this.app.screen.height / 2;
        this.bg.pivot.x = SIZE * WIDTH / 2;
        this.bg.pivot.y = SIZE * HEIGHT / 2;
        this.stage.sortableChildren = true;
        this.stage.addChild(this.bg);
    }

    initSound() {
        PIXI.sound.add({
            shot: "assets/audio/shot.mp3",
            explosion: "assets/audio/explosion.mp3",
            intro: {
                url: "assets/audio/intro.mp3",
                autoPlay: true,
                loaded() {
                    setTimeout(() => PIXI.sound.play("bg", {loop: true}), 4540);
                }
            },
            bg: {url: "assets/audio/bg.mp3", preload: true}
        });
    }

    initSocket(username, skin_id) {
        this.socket.emit("init", username, skin_id);

        this.socket.on("init", self_id => {
            console.log(`Successfully connected as player #${self_id}`);
            this.player.id = self_id;
        });

        this.socket.on("join", players_data => {
            for (const data of players_data) {
                console.log("New player #" + data[0]);
                this.addPlayer(new Player(data[0], data[1], data[2]));
            }
        });

        this.socket.on("leave", id => {
            this.players[id].dispose();
            delete this.players[id];
        });

        this.socket.on("upd", players_data => {
            for (const data of players_data) {
                const player = this.players[data[0]];

                if (!player)
                    continue;

                data.shift(1);
                player.unserialize(data);
            }
        });

        this.socket.on("shoot", data => {
            let bullet = new Bullet(data[0], data[4]);

            bullet.unserialize(data);
            this.stage.addChild(bullet.sprite);
            this.bullets.push(bullet);
        });

        this.socket.on("die", data => {
            const player_id = data[0];
            const bullet_id = data[1];

            this.players[player_id].explode();

            for (const bullet of this.bullets) {
                if (bullet.id == bullet_id) {
                    bullet.lifetime = 0;
                    return;
                }
            }
        });

        this.socket.on("disconnect", () => {
            if (this.player.isAlive)
                alert("Connection lost.");

            this.player.isAlive = false;

            setTimeout(() => { this.app.stop(); }, 2e3);

            PIXI.sound.stop("bg");

            this.finish?.();
        });

        setInterval(() => {
            this.socket.emit("upd", this.player.serialize());
        }, SRV_UPDATE_INTERVAL);
    }

    addPlayer(player) {
        if (this.players[player.id])
            return console.error("Error: duplicate player", player.id);

        this.players[player.id] = player;
        this.stage.addChild(player.container);
    }

    isKeyPressed(keys) {
        return keys.some(key => this.pressedKeys[key] || false);
    }

    update(dt) {
        this.stage.pivot.x    = this.player.pos.x;
        this.stage.pivot.y    = this.player.pos.y;
        this.stage.position.x = this.app.screen.width / 2;
        this.stage.position.y = this.app.screen.height / 2;

        if (this.isKeyPressed(CONTROLS.THRUST)) {
            this.player.vel.x += Math.sin(this.player.rot) * dt * ACCELERATION;
            this.player.vel.y -= Math.cos(this.player.rot) * dt * ACCELERATION;
        }

        if (this.isKeyPressed(CONTROLS.FIRE)) {
            let bullet = this.player.shoot(false, true);

            if (bullet) {
                this.stage.addChild(bullet.sprite);
                this.bullets.push(bullet);
                bullet.isMine = true;
                this.socket.emit("shoot", bullet.serialize());
                PIXI.sound.play("shot");
            }
        }
        this.player.setThrust(this.isKeyPressed(CONTROLS.THRUST));

        this.player.rot += this.isKeyPressed(CONTROLS.RIGHT) * dt * ROT_SPEED;
        this.player.rot -= this.isKeyPressed(CONTROLS.LEFT) * dt * ROT_SPEED;

        for (let player of Object.values(this.players))
            player.update(dt);

        for (let bullet of this.bullets) {
            bullet.update(dt);

            if (bullet.isMine) {
                for (let player of Object.values(this.players)) {
                    if (player != this.player && bullet.pos.clone().sub(player.pos).getDist() <= PLAYER_SIZE)
                        this.socket.emit("hit", bullet.id, player.id);
                }
            }
        }
        this.bullets = this.bullets.filter(b => (b.lifetime > 0));
    }
}

PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.LINEAR;

window.onload = () => {
    window.addEventListener("message", ({data}) => {
        const menu_el = document.getElementById("menu-div");

        if (menu_el.classList[0])
            return;

        menu_el.classList.add("hidden");
        window.focus();

        game = new Game(data.username, data.skin_id);

        game.finish = () => {
            menu_el.classList.remove("hidden");
        };
    })
}
