const MAX_SPEED     = 0.75;
const SRV_INTERVAL  = 100;
const BULLETS_BUFF  = 64;

class Player {
    constructor(id, username, skin_id = 0, pos = new Vector2()) {
        this.id             = id;
        this.username       = username;
        this.isAlive        = true;
        this.isMine         = !id;
        this.pos            = pos;
        this.vel            = new Vector2();
        this.rot            = 0;
        this.skinId         = skin_id;
        this.firingTimes    = [0, 0];
        this.minWpDelays    = [1e2, 1e4];
        this.container      = new PIXI.Container();
        this.sprite         = PIXI.Sprite.from(`assets/players/${skin_id}/ship.png`);
        this.thrustSprite   = PIXI.Sprite.from(`assets/thrust.png`);
        this.nameText       = new PIXI.Text(
            username,
            {
                fontFamily: "monospace",
                fontSize: 18,
                fill: this.isMine ? 0xffffff : 0xaaaaaa,
                align: "center"
            }
        );
        this.lastUpdTime    = new Date().getTime();
        this.lastBulletId   = 0;

        this.sprite.width   = 100;
        this.sprite.height  = 100;
        this.sprite.anchor.set(0.5, 0.6);
        this.thrustSprite.anchor.set(0.5, -0.2);
        this.nameText.anchor.set(0.5, 4.2);
        this.container.addChild(this.sprite);
        this.container.addChild(this.nameText);
        this.sprite.addChild(this.thrustSprite);
        if (!this.isMine) {
            this.f_pos  = this.pos.clone();
            this.t_pos  = this.pos.clone();
            this.f_rot  = 0;
            this.t_rot  = 0;
        }
        this.container.zIndex = 1e3;
    }

    setExplosionSprite() {
        let textures = [];

        for (let i = 1; i < 47; i++)
            textures.push(PIXI.Texture.from(`assets/explosion/${i}.png`));
        this.explosionSprite        = new PIXI.AnimatedSprite(textures);
        this.explosionSprite.loop   = false;
        this.explosionSprite.anchor.set(0.45);
        this.explosionSprite.play();
        this.container.addChild(this.explosionSprite);
        if (this.isMine)
            PIXI.sound.play("explosion");
        this.explosionSprite.onFrameChange = frame => {
            this.nameText.alpha = 1 - frame / 45;
            if (frame == 35)
                this.sprite.visible = false;
        };
        this.explosionSprite.onComplete = () => {
            this.explosionSprite.visible = false;
        };
    }

    setThrust(is_active = true) {
        this.thrustSprite.visible = is_active;
    }

    shoot(is_primary = true, check_cooldown = false) {
        const now = new Date().getTime();

        if (
            check_cooldown &&
            now - this.firingTimes[0 + is_primary] < this.minWpDelays[0 + is_primary]
        )
            return null;

        this.firingTimes[0 + is_primary] = now;
        this.lastBulletId = ++this.lastBulletId % BULLETS_BUFF;

        return new Bullet(
            this.id * 100 + this.lastBulletId,
            this.skinId,
            this.pos,
            this.vel,
            this.rot
        );
    }

    update(dt) {
        const now = new Date().getTime();

        if (!this.isAlive)
            return;

        this.vel.clampAbs(MAX_SPEED);

        if (!this.isMine && this.lastUpdTime + SRV_INTERVAL > now) {
            let ratio = (now - this.lastUpdTime) / SRV_INTERVAL;

            this.pos.x  = this.f_pos.x + (this.t_pos.x - this.f_pos.x) * ratio;
            this.pos.y  = this.f_pos.y + (this.t_pos.y - this.f_pos.y) * ratio;
            this.rot    = this.f_rot + (this.t_rot - this.f_rot) * ratio;
        } else {
            this.pos.add(this.vel.clone().mult(dt));
        }

        this.container.position.set(this.pos.x, this.pos.y);
        this.container.rotation = this.rot;
        this.nameText.rotation = -this.rot;
    }

    serialize() {
        return [
            this.pos.serialize(),
            this.vel.serialize(),
            Math.round(this.rot * 1e2) / 1e2,
            0 + this.thrustSprite.visible
        ];
    }

    unserialize(data) {
        if (
            data.length != 4 ||
            data[0].length != 2 ||
            data[1].length != 2
        )
            return false;

        this.pos        = this.t_pos.clone();
        this.rot        = this.t_rot;
        this.f_pos      = this.pos.clone();
        this.f_rot      = this.rot;
        this.t_pos.x    = parseFloat(data[0][0]);
        this.t_pos.y    = parseFloat(data[0][1]);
        this.t_rot      = parseFloat(data[2]);
        this.vel.x      = parseFloat(data[1][0]);
        this.vel.y      = parseFloat(data[1][1]);
        this.lastUpdTime = new Date().getTime();

        this.setThrust(data[3]);

        return true;
    }

    explode() {
        if (!this.isAlive)
            return;

        this.isAlive = false;
        this.setExplosionSprite();
    }

    dispose() {
        this.sprite.destroy();
        this.nameText.destroy();
        this.thrustSprite.destroy();
    }
}
